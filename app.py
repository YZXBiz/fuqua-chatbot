# We're importing necessary tools to build our application.
from flask import Flask, render_template, request
from bcr_api.bwproject import BWProject, BWUser
from bcr_api.bwresources import BWQueries
import datetime
import pandas as pd
import translate_bot
import text_process
import openai
import os

# This line creates a new web application.
app = Flask(__name__)

# This section is where the magic happens when you visit the main page of our application.
@app.route('/', methods=['GET', 'POST'])
def index():
    # When you submit the form on the main page, this part of the code is executed.
    if request.method == 'POST':  
        # The application logs into your Brandwatch account using your credentials.
        BWUser(
            token_path='credentials.txt',
            username="fuquamarketintel@duke.edu"
        )

        # Your account and project names are set here.
        YOUR_ACCOUNT = "fuquamarketintel@duke.edu"
        YOUR_PROJECT = 'Chinese Media Monitoring'

        # A project in your Brandwatch account is selected.
        project = BWProject(
            username=YOUR_ACCOUNT,
            project=YOUR_PROJECT,
            token_path='credentials.txt'
        )

        # The application fetches all queries (or search criteria) in the selected project.
        queries = BWQueries(project)

        # The application opens and reads the file containing the search criteria.
        with open('booleanquery.txt', 'r') as f:
            booleanquery = f.read()

        # The search criteria are uploaded to the Brandwatch project.
        queries.upload(
            name="2001192519",
            booleanQuery=booleanquery
        )
        
        # The application fetches the dates you've entered.
        start_date = request.form['start_date']
        end_date = request.form['end_date']
      
        # The application sets the time range for the search based on your entered dates.
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d').date() + datetime.timedelta(days=1)
        end_date = end_date.isoformat() + "T05:00:00"
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d').date().isoformat() + "T05:00:00"

        # The application fetches the posts that match the search criteria.
        filtered = queries.get_mentions(
            name="2001192519",
            startDate=start_date,
            endDate=end_date
        )

        # The search results are organized into a nice table.
        df = pd.DataFrame(filtered, columns=['date','originalUrl', 'title', 'fullText'])

        # The application logs into the OpenAI system.
        openai.api_key_path = 'openaikey.txt'

        # A translation bot is set up to translate Chinese to English.
        translator = translate_bot.CreateBot(system_prompt="""
            You possess excellent skills in translating Chinese to English. 
            Kindly provide me with the English translation without any elaboration or supplementary information. 
            Your sole duty is to translate the text and refrain from performing any other task.
        """)

        # The table of search results is processed and translated.
        df = text_process.process_data(df,translator)

        # The translated data is saved in a new Excel file.
        folder = 'output'
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = 'translated_data.xlsx'
        filepath = os.path.join(folder, filename)
        df.to_excel(filepath, index=False)

        # The application shows a success page to let you know that the file has been saved.
        message = f'The translated data has been saved to {filepath}'
        return render_template('success.html', message=message)

    else:
        # If you've just arrived at the main page and haven't submitted the form, 
        # the application shows you the search page.
        return render_template('index.html')

# This line is the starting point of our application. It's like turning the key to start a car.
if __name__ == '__main__':
    app.run(debug=True)

