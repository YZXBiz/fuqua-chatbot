import openai

#create a class for translation
class CreateBot:
    
    def __init__(self, system_prompt):
        '''
        system_prompt: [str] Describes context for Chat Assistant
        '''
        self.system = system_prompt
        self.messages = [{"role": "system", "content": system_prompt}]
        
    def translate(self,text):
        '''
        Translate bot
        '''
        
        # Add to messages/dialogue history
        self.messages.append({'role':'user','content':text})

        #Send to ChatGPT and get response
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",         # This specifies the model to be used. In this case, it's GPT-3.5 Turbo.
            messages=self.messages,         # This is a list of messages to be processed. Each message has two properties - "role" (which can be 'system', 'user', or 'assistant') and "content" (the content of the message).
            temperature=0.2,               # This controls randomness in the output. A higher value (closer to 1) makes the output more random, while a lower value (closer to 0) makes it more deterministic.
            #max_tokens=2000,                # This is the maximum length of the output. If the output exceeds this limit, it will be cut off.
            #top_p=1.0,                     # This is related to "nucleus sampling", a method for generating text. A value of 1.0 means all tokens with a non-zero probability can be chosen, making the output more diverse.
            #frequency_penalty=0.0,         # This penalizes more frequent tokens. A higher value (up to 1) makes common words less likely to appear.
            #presence_penalty=0.0           # This penalizes new tokens. A higher value (up to 1) makes the model less likely to introduce new topics.
            # uncomment above if you want to tune them
        )

        # Get content of assistant reply
        content = response['choices'][0]['message']['content']
        return content