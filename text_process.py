def process_data(df, translator):
    """
    Divides the full text and title into smaller pieces based on length and translates each piece using the provided translator object.

    Parameters:
        df (Pandas DataFrame): The DataFrame to process.
        translator (TranslateBot): The translator object to use for translation.

    Returns:
        The processed DataFrame with translated text.
    """
    for i, row in df.iterrows():
        title = row['title']
        full_text = row['fullText']

        # Check if either text exceeds the length limit [you can change 1000 accordingly if you have better models, gpt4, 5 etc..]
        if len(title) > 1000 or len(full_text) > 1000:
            df.loc[i, 'translated_title'] = 'skip because text is too long'
            df.loc[i, 'translated_fullText'] = 'skip because text is too long'
            continue  # skip to next row immediately
            
        # Translate title and full_text
        translated_title = translator.translate(title)
        translated_full_text = translator.translate(full_text)
        
        # Store the translated text in new columns
        df.loc[i, 'translated_title'] = translated_title
        df.loc[i, 'translated_fullText'] = translated_full_text
        
    return df
