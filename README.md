# Translation Bot
Welcome to our Translation Chatbot project! This tool is designed to translate text comments from Chinese forums into English.

# Description of Each File
- `app.py`: This is the main Python file that runs the translation application, where the actual translation takes place.
- `code.ipynb`: This is an IPython notebook for project testing and reviewing brandwatch information and credentials. 
- `credentials.txt`: This file contains our account ID and the Brandwatch API, crucial for the functioning of the bot.
- `output`: This directory is where the translated files are stored [note: the newest file will replace the previous one].
- `readme.md`: This file contains all instructions related to the project.
- `templates`: This directory contains HTML files for the front-end interface of the application.
- `text_process.py`: This Python file handles data length processing. Currently, it's set to skip posts with over 1000 words due to the token limit of our GPT model. You can modify this as needed.
- `translate_bot.py`: This file contains the GPT model used for translations. You can swap out the model as needed (currently using GPT-3.5).
- `booleanquery.txt`: This is the query file used to extract text data from Brandwatch. You can modify it if you want to extract different texts. For example, you might want to include keywords related to environmental sustainability, etc. The text data ultimately comes from the query that Brandwatch uses to extract text data from various social media platforms.
- `requirements.yml`: If you want to deploy the project, this file includes the versions of libraries/packages that I used for chatbot.
  
# Important Things to Note
- **Model**: A superior model results in more accurate translations and has a longer translation limit. You can modify the GPT model used (currently GPT-3.5) in the `translate_bot.py` file.
- **Prompt Engineering**: You can tailor the prompts to suit your particular needs. This is done in the `app.py` file where the `system_prompt` is defined.
- **Translation Bot**: For more details on how to tweak the bot's parameters (like `temperature`, `max_tokens`, etc.), refer to the `openai.ChatCompletion.create` method in the `translate_bot.py` file.
- **Code**: All the code is written in Python and is thoroughly documented. If you need to understand it better, feel free to consult Chat-GPT.

# How to run (locally)
- Ensure you have Python installed on your system 
- Install the required Python libraries by running the following command in your terminal or command prompt: `pip install -r requirements.txt`
- Open the terminal or command prompt and navigate to the project directory
- Run the following command to start the translation bot: `python app.py`
- The translation bot application will be accessible at `http://localhost:5000` in your web browser.

# Contact
If you have any questions, feel free to reach out:
**Jackson Yang**
- Personal Email: yzhuox721@gmail.com
- Duke Email: zy157@duke.edu


